
public class EvenOdd {

	boolean status = true;

	synchronized void even() {

		if (status) {
			try {

				wait();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		for (int i = 2; i <= 100; i++) {

			if (status) {
				try {
					wait();
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
			}
			
				if (i % 2 == 0) {
					System.out.println("Even Thread" +i);
				}

				status = true;

				notify();
			}

		
		
	}

	synchronized void odd() {
		if (!status) {
			try {

				wait();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int i = 1; i < 100; i++) {
			if (!status) {

				try {

					wait();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (i % 2 != 0) {
				System.out.println("Odd Thread" +i);
			}
			status = false;

			notify();

		}
	
	}

}
